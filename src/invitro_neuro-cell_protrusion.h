// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_CELL_PROTRUSION_H_
#define INVITRO_NEURO_CELL_PROTRUSION_H_
// =============================================================================
#include "invitro_neuro-global.h"
#include "invitro_neuro-biochemical.h"
#include "neuroscience/new_agent_event/split_neurite_element_event.h"
#include "neuroscience/new_agent_event/neurite_branching_event.h"
#include "./obstacles.h"
// =============================================================================
namespace bdm {
// =============================================================================
// class forward declarations:
class InVitroNeuro_Cell;
// =============================================================================
class InVitroNeuro_CellProtrusion : public neuroscience::NeuriteElement {
BDM_AGENT_HEADER(InVitroNeuro_CellProtrusion, neuroscience::NeuriteElement, 1);
//
public:
  InVitroNeuro_CellProtrusion ()
    : neuroscience::NeuriteElement() {
    cell_ = 0;
    age_ = 1;
    can_branch_ = can_sprout_ = true;
    params_ = 0; // nullify pointer...
  }
  //
  void Initialize (const NewAgentEvent& event) override {
    Base::Initialize(event);
    if (auto* mother = dynamic_cast<InVitroNeuro_CellProtrusion*>(event.existing_agent))
    {
      if (event.GetUid() == bdm::neuroscience::SplitNeuriteElementEvent::kUid) {
        cell_ = mother->GetCell();
        SetAge(); // ...age cannot be inherited
        can_branch_ = mother->GetCanBranch();
        can_sprout_ = mother->GetCanSprout();
        params_ = mother->params_; // copy parameters pointer...
      } else if (event.GetUid() == bdm::neuroscience::NeuriteBranchingEvent::kUid) {
        cell_ = mother->GetCell();
        SetAge(); // ...age cannot be inherited
        can_branch_ = mother->GetCanBranch();
        can_sprout_ = mother->GetCanSprout();
        params_ = mother->params_; // copy parameters pointer...
      } else {
        bdm::Log::Fatal("InVitroNeuro_CellProtrusion::Initialize",
            "error @line "+std::to_string(__LINE__)+"; an exception is caught");
      }
    }
  }
  //
  void SetCell (const InVitroNeuro_Cell* c) const { cell_ = c; }
  const InVitroNeuro_Cell* GetCell () const { return cell_; }
  //
  void SetAge (unsigned int a =1) { age_ = a; }
  int  GetAge () const { return age_; }
  void IncrementAge () { age_++; }
  //
  int GetTimeRepeats () const;
  //
  void SetCanBranch (bool branches) { can_branch_ = branches; }
  bool GetCanBranch () { return can_branch_; }
  //
  void SetCanSprout (bool sprouts) { can_sprout_ = sprouts; }
  bool GetCanSprout () { return can_sprout_; }
  //
  void SetParametersPointer (Parameters* p) { params_ = p; }
  Parameters* params () const { return params_; }
  //
  bool CheckPositionValidity ();
  bool CheckTimeWindow () const;
  void CheckOutGrowth ();
  void CheckBranching ();
  void CheckState ();
  void Set2Delete () const;
//
private:
  // pointer to the cell that this protrusion is associated with
  mutable
  const InVitroNeuro_Cell* cell_ = 0;
  // cell protrusion age (non-fractional time)
  int age_ = 1;
  // flags to designate (individual) protrusion behaviour
  mutable
  bool can_branch_, can_sprout_;
  // pointer to all simulation parameters
  mutable
  Parameters* params_ = 0;
};
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_CELL_PROTRUSION_H_
// =============================================================================
