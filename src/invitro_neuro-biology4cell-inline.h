// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_BIOLOGY4CELL_INLINE_H_
#define INVITRO_NEURO_BIOLOGY4CELL_INLINE_H_
// =============================================================================
#include "invitro_neuro-cell.h"
// =============================================================================
namespace bdm {
// =============================================================================
inline
void InVitroNeuro_Biology4Cell_10::Run (Agent* a) {
  if (auto* cell = dynamic_cast<InVitroNeuro_Cell*>(a))
  {
    // firstly, we should check if cell is inside the simulation domain
    if (!cell->CheckPositionValidity()) {
      cell->Set2DeleteProtrusions();
      cell->RemoveBehavior(this);
      cell->RemoveFromSimulation();
      return;
    }
    // we check for cell apoptosis
    if (cell->CheckApoptosis()) {
      cell->Set2DeleteProtrusions();
      cell->RemoveBehavior(this);
      cell->RemoveFromSimulation();
      return;
    }
    // simply update the cell age
    cell->IncrementAge();
    // cell produces/consumes substances
    cell->RunBiochemics();
    // now check if cell can migrate
    if (cell->CheckMigration()) {
      if (!cell->CheckPositionValidity()) {
        cell->Set2DeleteProtrusions();
        cell->RemoveBehavior(this);
        cell->RemoveFromSimulation();
        return;
      }
    }
    // then, check if cell can transform or if it can polarize
    if (cell->CheckTransformation()) return;
    cell->CheckPolarization();
    cell->CheckProtrusion();
    // check if cell can grow
    if (cell->CheckGrowth()) return;
    // check if cell can divide (summetrically or unsymmetrically)
    if (cell->CheckTransformationAndDivision()) return;
    if (cell->CheckAsymmetricDivision()) return;
    if (cell->CheckDivision()) return;
    // finally, we check for cell apoptosis due to aging
    if (cell->CheckApoptosisAging()) {
      cell->Set2DeleteProtrusions();
      cell->RemoveBehavior(this);
      cell->RemoveFromSimulation();
      return;
    }
    // ...end of mechanisms list for cell behavior
  }
  else
  {
    bdm::Log::Fatal("bdm::InVitroNeuro_Biology4Cell_10::Run",
      "error @line "+std::to_string(__LINE__));
  }
}
// -----------------------------------------------------------------------------
inline
void InVitroNeuro_Biology4Cell_11::Run (Agent* a) {
  if (auto* cell = dynamic_cast<InVitroNeuro_Cell*>(a))
  {
    if (InVitroNeuro_Cell::Phase::Ap==cell->GetPhase()) {
      cell->IncrementAge();
      //
      if (cell->CheckAfterApoptosis()) {
        cell->Set2DeleteProtrusions();
        cell->RemoveBehavior(this);
        cell->RemoveFromSimulation();
        return;
      }
      return;
    }
    // firstly, we should check if cell is inside the simulation domain
    if (!cell->CheckPositionValidity()) {
      cell->Set2DeleteProtrusions();
      cell->RemoveBehavior(this);
      cell->RemoveFromSimulation();
      return;
    }
    // simply update the cell age
    cell->IncrementAge();
    // cell produces/consumes substances
    cell->RunBiochemics();
    //
    if (InVitroNeuro_Cell::Phase::Di==cell->GetPhase())
      if (cell->CheckQuiescenceAfterDivision()) {
        return;
      }
    // we check for cell apoptosis
    if (cell->CheckApoptosis()) {
      cell->SetAge(); // reset age (time) counter
      cell->SetPhase(InVitroNeuro_Cell::Phase::Ap);
      return;
    }
    // check if cell can polarize
    if (cell->CheckPolarization()) {
      if (!cell->CheckPositionValidity()) {
        cell->Set2DeleteProtrusions();
        cell->RemoveBehavior(this);
        cell->RemoveFromSimulation();
        return;
      }
    }
    // now check if cell can migrate
    if (cell->CheckMigration()) {
      if (!cell->CheckPositionValidity()) {
        cell->Set2DeleteProtrusions();
        cell->RemoveBehavior(this);
        cell->RemoveFromSimulation();
        return;
      }
    }
    // check if cell can divide (summetrically or unsymmetrically)
    if (InVitroNeuro_Cell::Phase::G1==cell->GetPhase()||
        InVitroNeuro_Cell::Phase::Sy==cell->GetPhase())
      if (cell->CheckDivision() || cell->CheckAsymmetricDivision()) {
        cell->SetAge(); // reset age (time) counter
        cell->SetPhase(InVitroNeuro_Cell::Phase::Di);
        return;
      }
    // check if cell can grow
    if (InVitroNeuro_Cell::Phase::G1==cell->GetPhase()||
        InVitroNeuro_Cell::Phase::Sy==cell->GetPhase())
      if (cell->CheckGrowth()) {
        cell->SetPhase(InVitroNeuro_Cell::Phase::G1);
        return;
      }
    // finally, we check for cell apoptosis due to aging
    if (InVitroNeuro_Cell::Phase::G1==cell->GetPhase()||
        InVitroNeuro_Cell::Phase::Sy==cell->GetPhase()||
        InVitroNeuro_Cell::Phase::G2==cell->GetPhase())
      if (cell->CheckApoptosisAging()) {
        cell->SetAge(); // reset age (time) counter
        cell->SetPhase(InVitroNeuro_Cell::Phase::Ap);
        return;
      }
    //
    cell->SetPhase(InVitroNeuro_Cell::Phase::Sy);
  }
  else
  {
    bdm::Log::Fatal("bdm::InVitroNeuro_Biology4Cell_11::Run",
      "error @line "+std::to_string(__LINE__));
  }
}
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_BIOLOGY4CELL_INLINE_H_
// =============================================================================
