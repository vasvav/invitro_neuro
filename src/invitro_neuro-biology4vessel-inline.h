// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_BIOLOGY4VESSEL_INLINE_H_
#define INVITRO_NEURO_BIOLOGY4VESSEL_INLINE_H_
// =============================================================================
#include "invitro_neuro-vessel.h"
// =============================================================================
namespace bdm {
// =============================================================================
inline
void InVitroNeuro_Biology4Vessel::Run (Agent* a) {
  if (auto* vessel = dynamic_cast<InVitroNeuro_Vessel*>(a))
  {
    // firstly, we should check if vessel is inside the simulation domain
    if (!vessel->CheckPositionValidity()) {
      vessel->RemoveBehavior(this);
      return;
    }
    // simply update the vessel age
    vessel->IncrementAge();
    // vessel produces/consumes substances
    vessel->RunBiochemics();
    // check if vessel can grow
    vessel->CheckGrowth();
    // othewise, check if vessel has sprouted or branched out
    if (vessel->CheckSprouting()) return;
    if (vessel->CheckBranching()) return;
    // ...end of mechanisms list for vessel behavior
  }
  else
  {
    bdm::Log::Fatal("bdm::InVitroNeuro_Biology4Vessel::Run",
      "error @line "+std::to_string(__LINE__));
  }
}
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_BIOLOGY4VESSEL_INLINE_H_
// =============================================================================
