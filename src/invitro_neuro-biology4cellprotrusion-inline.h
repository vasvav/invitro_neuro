// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_BIOLOGY4CELLPROTRUSION_INLINE_H_
#define INVITRO_NEURO_BIOLOGY4CELLPROTRUSION_INLINE_H_
// =============================================================================
#include "invitro_neuro-cell_protrusion.h"
// =============================================================================
namespace bdm {
// =============================================================================
inline
void InVitroNeuro_Biology4CellProtrusion::Run (Agent* a) {
  if (auto* protrusion = dynamic_cast<InVitroNeuro_CellProtrusion*>(a))
  {
    // check if corresponding cell (to this protrusion) exists in simulation
    auto* cell = protrusion->GetCell();
    if (nullptr==cell) {
      protrusion->RemoveBehavior(this);
      protrusion->RemoveFromSimulation();
      return;
    }
    // simply update the cell protrusion age
    protrusion->IncrementAge();
    // check if cell age is within appropriate time-window to allow development
    if (!protrusion->CheckTimeWindow()) return;
    //
    const int n_repeat = protrusion->GetTimeRepeats();
    for (int repeat=0; repeat<n_repeat; repeat++)
    {
      // firstly, we should check if protrusion is inside the simulation domain
      if (!protrusion->CheckPositionValidity()) {
        protrusion->RemoveBehavior(this);
        return;
      }
      // check if cell protrusion outgrows or/and branches
      protrusion->CheckOutGrowth();
      protrusion->CheckBranching();
    }
    //
    protrusion->CheckState();
  }
  else
  {
    bdm::Log::Fatal("bdm::InVitroNeuro_Biology4CellProtrusion::Run",
      "error @line "+std::to_string(__LINE__));
  }
}
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_BIOLOGY4CELLPROTRUSION_INLINE_H_
// =============================================================================
