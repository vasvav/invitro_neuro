/*******************************************************************************

    Copyright (C) 2020-2022 Vasileios Vavourakis (vasvav@gmail.com)

    This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by the Free
    Software Foundation, version 2 of the License. However, this program
    employs the BioDynaMo platform (https://biodynamo.org/) which is licensed
    under the Apache License, version 2.0.

    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
    more details.

    You should have received a copy of the GNU General Public License along with
    this program; if not, please write to:
        Free Software Foundation, Inc.,
        59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

 ******************************************************************************/

// =============================================================================
#include "invitro_neuro.h"
// =============================================================================
int main (int argc, const char* argv[]) {
  std::cout << std::endl;
  std::cout << "Project \"invitro_neuro\" started... [";
  for (int i=0; i<argc; i++)
    std::cout << ' ' << argv[i];
  std::cout << "]" << std::endl;
  //
  std::string fname = "input.csv";
  int seed = 0;
  switch (argc) {
    case 1 :
      break;
    case 2 :
      fname = argv[1];
      break;
    case 3 :
      fname = argv[1];
      seed = abs(std::atoi(argv[2]));
      break;
    default :
      return 1;
  }
  //
  return invitro_neuro(fname, seed);
}
// =============================================================================
