// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_CELL_H_
#define INVITRO_NEURO_CELL_H_
// =============================================================================
#include "invitro_neuro-global.h"
#include "invitro_neuro-biology.h"
#include "invitro_neuro-biochemical.h"
#include "invitro_neuro-cell_protrusion.h"
#include "./obstacles.h"
#include "./io_flux.h"
// =============================================================================
namespace bdm {
// =============================================================================
class InVitroNeuro_Cell : public neuroscience::NeuronSoma {
BDM_AGENT_HEADER(InVitroNeuro_Cell, neuroscience::NeuronSoma, 1);
//
public:
  // local enumerator that monitors the phase of a cell's circle
  enum Phase {
    Ap =-1,
    I0 =0, G1 =1, Sy =2, G2 =3, Di =4, Tr =5
  };
//
public:
  InVitroNeuro_Cell () {}
  explicit InVitroNeuro_Cell (int p, const Double3& xyz)
    : neuroscience::NeuronSoma(xyz) {
    phenotype_ = p;
    phase_ = InVitroNeuro_Cell::Phase::I0;
    age_ = 1;
    polarize_ = eye();
    can_apoptose_ = can_grow_ = can_divide_ = can_migrate_ = can_transform_ = can_polarize_ = can_protrude_ = false;
    trail_ = 0.0;
    n_divisions_ = n_trasformations_ = n_protrusions_ = 0;
    params_ = 0; // nullify pointer...
  }
  //
  void Initialize (const NewAgentEvent& event) override {
    NeuronSoma::Initialize(event);
    // if cell divides then attributes have to be initialized
    if (auto* mother = dynamic_cast<InVitroNeuro_Cell*>(event.existing_agent))
    {
      if (event.GetUid() == bdm::CellDivisionEvent::kUid) {
        phenotype_ = mother->GetPhenotype();
        phase_ = InVitroNeuro_Cell::Phase::I0;
        SetAge(); // ...age cannot be inherited
        polarize_ = mother->GetPolarization();
        can_apoptose_  = mother->GetCanApoptose();
        can_grow_      = mother->GetCanGrow();
        can_divide_    = mother->GetCanDivide();
        can_migrate_   = mother->GetCanMigrate();
        can_transform_ = mother->GetCanTransform();
        can_polarize_  = mother->GetCanPolarize();
        can_protrude_  = mother->GetCanProtrude();
        ResetTrail(); // ...trail cannot be inherited
        n_divisions_ = 0; mother->IncrementNumberOfDivisions();
        n_trasformations_ = 0; // ...index is initialized
        n_protrusions_ = 0; // ...index is initialized
        params_ = mother->params_; // copy parameters pointer...
        CheckAndFixDiameter(); mother->CheckAndFixDiameter();
      } else {
        bdm::Log::Fatal("InVitroNeuro_Cell::Initialize",
          "error @line "+std::to_string(__LINE__)+"; an exception is caught");
      }
    }
  }
  //
  void SetPhenotype (int p) { phenotype_ = p; }
  int GetPhenotype () const { return phenotype_; }
  //
  void SetPhase (int p) { phase_ = static_cast<InVitroNeuro_Cell::Phase>(p); }
  int GetPhase () const { return phase_; }
  //
  void SetAge (unsigned int a =1) { age_ = a; }
  int  GetAge () const { return age_; }
  void IncrementAge () { age_++; }
  //
  void SetPolarization (const bdm::Double3x3& p) { polarize_ = p; }
  const bdm::Double3x3& GetPolarization () const { return polarize_; }
  const double& GetPolarization (size_t i, size_t j) const { return polarize_[i][j]; }
  //
  void SetCanApoptose (bool apoptoses) { can_apoptose_ = apoptoses; }
  bool GetCanApoptose () const { return can_apoptose_; }
  //
  void SetCanGrow (bool grows) { can_grow_ = grows; }
  bool GetCanGrow () const { return can_grow_; }
  //
  void SetCanDivide (bool divides) { can_divide_ = divides; }
  bool GetCanDivide () const { return can_divide_; }
  //
  void SetCanMigrate (bool migrates) { can_migrate_ = migrates; }
  bool GetCanMigrate () const { return can_migrate_; }
  //
  void SetCanTransform (bool transforms) { can_transform_ = transforms; }
  bool GetCanTransform () const { return can_transform_; }
  //
  void SetCanPolarize (bool polarizes) { can_polarize_ = polarizes; }
  bool GetCanPolarize () const { return can_polarize_; }
  //
  void SetCanProtrude (bool protrudes) { can_protrude_ = protrudes; }
  bool GetCanProtrude () const { return can_protrude_; }
  //
  void ResetTrail () { trail_ = 0.0; }
  void UpdateTrail (double d) { trail_ += d; }
  double GetTrail () const { return trail_; }
  //
  const bdm::Double3& GetActiveDisplacement  () const { return active_displacement_; }
  const bdm::Double3& GetPassiveDisplacement () const { return passive_displacement_; }
  const bdm::Double3 GetDisplacement () const { return active_displacement_+passive_displacement_; }
  const double GetDisplacement (size_t i) const { return active_displacement_[i]+passive_displacement_[i]; }
  //
  void IncrementNumberOfDivisions () { ++n_divisions_; }
  int GetNumberOfDivisions () const { return n_divisions_; }
  //
  void IncrementNumberOfTrasformations () { ++n_trasformations_; }
  int GetNumberOfTrasformations () const { return n_trasformations_; }
  //
  void IncrementNumberOfProtrusions () { ++n_protrusions_; }
  int GetNumberOfProtrusions () const { return n_protrusions_; }
  //
  void SetParametersPointer (Parameters* p) { params_ = p; }
  Parameters* params () const { return params_; }
  //
  void RunBiochemics ();
  bool CheckPositionValidity ();
  bool CheckApoptosisAging ();
  bool CheckApoptosis ();
  bool CheckAfterApoptosis ();
  bool CheckQuiescenceAfterDivision ();
  bool CheckMigration ();
  bool CheckTransformation ();
  bool CheckPolarization ();
  bool CheckProtrusion ();
  bool CheckGrowth ();
  bool CheckTransformationAndDivision ();
  bool CheckAsymmetricDivision ();
  bool CheckDivision ();
  void Set2DeleteProtrusions ();
  //
//
private:
  //
  void CheckAndFixDiameter ();
  bool CheckProtrusionAxis (Double3 axis);
  //
//
private:
  // index to designate the cell phenotype
  int phenotype_ = 0; // WARNING: phenotype ID must be >=0
  // cell circle phase
  InVitroNeuro_Cell::Phase phase_ = InVitroNeuro_Cell::Phase::I0;
  // cell age (non-fractional time)
  int age_ = 1;
  // cell polarization axes
  bdm::Double3x3 polarize_ = eye();
  // flags to designate (individual) cell behaviour
  bool can_apoptose_, can_grow_, can_divide_, can_migrate_, can_transform_, can_polarize_, can_protrude_;
  // total cell trail (displacement) between user-defined time points
  double trail_ = 0.0;
  bdm::Double3 active_displacement_ = {0.0, 0.0, 0.0};
  bdm::Double3 passive_displacement_ = {0.0, 0.0, 0.0};
  // index to keep track of the (individual) cell divisions & trasformations
  // and total number of filopodium or/and neurite (outgrowth) protrusions
  int n_divisions_ = 0, n_trasformations_ = 0, n_protrusions_ = 0;
  // pointer to all simulation parameters
  mutable
  Parameters* params_ = 0;
  // list of cell protrusions (filopodia or neurites)
  std::vector<Double3> protrusions_;
};
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_CELL_H_
// =============================================================================
