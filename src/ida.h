// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVERSE_DISTANCE_ALGORITHM_H_
#define INVERSE_DISTANCE_ALGORITHM_H_
// =============================================================================
#include "invitro_neuro-global.h"
// =============================================================================
class InverseDistanceAlgorithm {
//
public:
  InverseDistanceAlgorithm (double c, const std::vector<bdm::Double3>& pnt) : coeff_(c) { this->init(pnt); }
  explicit InverseDistanceAlgorithm (double c =1.0) : is_init_(false), coeff_(c), pnt_(0) {}
  ~InverseDistanceAlgorithm () {}
  //
  inline
  void init (const std::vector<bdm::Double3>& pnt) {
    this->is_init_ = true;
    this->pnt_ = &pnt;
  }
  //
  inline
  double interpolate (const bdm::Double3& apnt, const std::vector<double>& input) const {
    const int npnt = this->pnt_->size();
    // sanity check
    if ( npnt != input.size() )
      bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__));
    // calculate the weights
    std::vector<double> phi;
    this->calculate_(apnt, phi);
    // ...and now interpolate the input data
    double out;
    out = 0.0;
    for (int i=0; i<npnt; i++)
      out += phi[i] * input[i];
    return out;
  }
//
private:
  inline
  void calculate_ (const bdm::Double3& apnt, std::vector<double>& phi) const
  {
      const int npnt = this->pnt_->size();
      std::vector<double> weights(npnt);
      for (int i=0; i<npnt; i++)
      {
          const bdm::Double3 delta = (*this->pnt_)[i] - apnt;
          double d = sqrt(delta[0]*delta[0]+delta[1]*delta[1]+delta[2]*delta[2]);
          weights[i] = 1.0 / pow(d, this->coeff_);
      }
      const double weights_tot = accumulate(weights.begin(), weights.end(), 0.0);
      // calculate the normalized weights
      phi.resize(npnt);
      for (int i=0; i<npnt; i++)
          phi[i] = weights[i] / weights_tot;
  }
//
private:
    bool is_init_;
    // (reciprocal) exponent of the inverse distance weight function
    double coeff_;
    // input point (cloud) used to interpolate the data
    const std::vector<bdm::Double3>* pnt_;
};
// =============================================================================
#endif // INVERSE_DISTANCE_ALGORITHM_H_
// =============================================================================
