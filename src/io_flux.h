// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef IOFLUX_H_
#define IOFLUX_H_
// =============================================================================
#include "invitro_neuro-global.h"
#include "core/container/math_array.h"
// =============================================================================
class SurfaceSTL {
public:
  struct Triangle {
    bdm::Double3 vertex_0, vertex_1, vertex_2;
    bdm::Double3 normal;
  };
//
public:
  SurfaceSTL () : phenotype(0), time_step(1) {}
  ~SurfaceSTL () {}
  //
  inline
  void init (int p, int DT, const std::vector<double>& F, const std::string& fname) {
    this->phenotype = p;
    this->time_step = DT;
    this->time_parameters = F;
    // now open the STL file to process
    std::ifstream fin(fname);
    if (!fin)
      bdm::Log::Fatal(std::string(__FILE__),"could not open file "+fname);
    std::string s;
    // read STL header and confirm it's valid
    {
      std::getline(fin, s);
      const std::vector<std::string> parsed_s = extract_words_vector(s);
      if ("solid"!=parsed_s[0])
        bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__));
    }
    //
    while ( true ) {
      bdm::Double3 v0, v1, v2, n;
      // read the normal or check if you have reached the end of the STL file
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        // check if you have almost finished reading the file
        if ("endsolid"==parsed_s[0]) break;
        // ...otherwise read this triangle, first the normal vector
        if ("facet"!=parsed_s[0] || "normal"!=parsed_s[1] || 5!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
        n = { std::stod(parsed_s[2]), std::stod(parsed_s[3]), std::stod(parsed_s[4]) };
      }
      //
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        if ("outer"!=parsed_s[0] || "loop"!=parsed_s[1] || 2!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
      }
      // read the 1st vertex
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        if ("vertex"!=parsed_s[0] || 4!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
        v0 = { std::stod(parsed_s[1]), std::stod(parsed_s[2]), std::stod(parsed_s[3]) };
      }
      // read the 2nd vertex
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        if ("vertex"!=parsed_s[0] || 4!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
        v1 = { std::stod(parsed_s[1]), std::stod(parsed_s[2]), std::stod(parsed_s[3]) };
      }
      // read the 3rd vertex
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        if ("vertex"!=parsed_s[0] || 4!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
        v2 = { std::stod(parsed_s[1]), std::stod(parsed_s[2]), std::stod(parsed_s[3]) };
      }
      //
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        if ("endloop"!=parsed_s[0] || 1!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
      }
      //
      {
        std::getline(fin, s);
        const std::vector<std::string> parsed_s = extract_words_vector(s);
        if ("endfacet"!=parsed_s[0] || 1!=parsed_s.size())
          bdm::Log::Fatal(std::string(__FILE__),"error @line "+std::to_string(__LINE__)+" / "+s);
      }
      // completed reading this triangle, now process the data
      SurfaceSTL::Triangle tri3;
      tri3.vertex_0 = v0;
      tri3.vertex_1 = v1;
      tri3.vertex_2 = v2;
      // outward unit normal vector
      tri3.normal = normalize(n);
      // load this triangle into member container
      triangle.push_back(tri3);
    }
    // now close the file stream
    fin.close();
  }
//
public:
  // list of all triangles
  std::vector<SurfaceSTL::Triangle> triangle;
  //
  int phenotype;
  // time interval between successive cell flux insertions
  int time_step;
  // list of time parameters defining the cell flux pattern
  std::vector<double> time_parameters;
};
// =============================================================================
class SimulationIOFlux {
public:
  SimulationIOFlux () {}
  ~SimulationIOFlux () {}
  //
  inline
  void clear () { surface.clear(); }
//
public:
  std::vector<SurfaceSTL> surface;
};
// =============================================================================
#endif // IOFLUX_H_
// =============================================================================
