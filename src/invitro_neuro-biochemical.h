// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_BIOCHEMICAL_H_
#define INVITRO_NEURO_BIOCHEMICAL_H_
// =============================================================================
#include <set>
#include "biodynamo.h"
// =============================================================================
namespace bdm {
// =============================================================================
enum InVitroNeuro_Biochemical {
  N_A = -1,
  RAD = 0,
  OH_ = 1, O2 = 2, O3 = 3, H2O = 4, H2O2 = 5, N2 = 6, NO_ = 7, NO2 = 8, NO3 = 9,
  Gluc = 20,
  VEGF = 21, PDGF = 22, PlGF = 23, Ang1 = 24, Ang2 = 25, EGF = 26, TGFa = 27, TGFb = 28, bFGF = 29,
  TNF  = 41,
  NGF  = 61, BDNF = 62,
  Drug_1 = 101, Drug_2 = 102, Drug_3 = 103,
  ECM = 999
};
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_BIOCHEMICAL_H_
// =============================================================================
