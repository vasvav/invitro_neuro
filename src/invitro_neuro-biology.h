// =============================================================================
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// =============================================================================

// =============================================================================
#ifndef INVITRO_NEURO_BIOLOGY_H_
#define INVITRO_NEURO_BIOLOGY_H_
// =============================================================================
#include "core/behavior/behavior.h"
// =============================================================================
namespace bdm {
// =============================================================================
struct InVitroNeuro_Biology4Cell_10 : public Behavior {
BDM_BEHAVIOR_HEADER(InVitroNeuro_Biology4Cell_10, Behavior, 1);
//
public:
  InVitroNeuro_Biology4Cell_10 () { AlwaysCopyToNew(); }
  virtual ~InVitroNeuro_Biology4Cell_10 () {}
  //
  void Initialize (const NewAgentEvent& event) override {
    Base::Initialize(event);
  }
  //
  void Run (Agent* a) override;
  //
};
// -----------------------------------------------------------------------------
struct InVitroNeuro_Biology4Cell_11 : public Behavior {
BDM_BEHAVIOR_HEADER(InVitroNeuro_Biology4Cell_11, Behavior, 1);
//
public:
  InVitroNeuro_Biology4Cell_11 () { AlwaysCopyToNew(); }
  virtual ~InVitroNeuro_Biology4Cell_11 () {}
  //
  void Initialize (const NewAgentEvent& event) override {
    Base::Initialize(event);
  }
  //
  void Run (Agent* a) override;
  //
};
// -----------------------------------------------------------------------------
struct InVitroNeuro_Biology4Vessel : public Behavior {
BDM_BEHAVIOR_HEADER(InVitroNeuro_Biology4Vessel, Behavior, 1);
//
public:
  InVitroNeuro_Biology4Vessel () { AlwaysCopyToNew(); }
  virtual ~InVitroNeuro_Biology4Vessel () {}
  //
  void Initialize (const NewAgentEvent& event) override {
    Base::Initialize(event);
  }
  //
  void Run (Agent* a) override;
  //
};
// -----------------------------------------------------------------------------
struct InVitroNeuro_Biology4CellProtrusion : public Behavior {
BDM_BEHAVIOR_HEADER(InVitroNeuro_Biology4CellProtrusion, Behavior, 1);
//
public:
  InVitroNeuro_Biology4CellProtrusion () { AlwaysCopyToNew(); }
  virtual ~InVitroNeuro_Biology4CellProtrusion () {}
  //
  void Initialize (const NewAgentEvent& event) override {
    Base::Initialize(event);
  }
  //
  void Run (Agent* a) override;
  //
};
// =============================================================================
} // namespace bdm
// =============================================================================
#endif // INVITRO_NEURO_BIOLOGY_H_
// =============================================================================
