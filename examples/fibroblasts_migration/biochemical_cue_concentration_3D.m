clear, clc, close all

S_min = -25.0;
S_max = +25.0;
N = 20;
DS = (S_max - S_min) / N;

Field_min = 0.00e-0;
Field_max = 1.00e-5;

fileID = fopen('bFGF.dat', 'w');

fprintf(fileID, '%d\n',(N-1)^3);
for I = 1 : N-1
    X = S_min + DS * I;
    for J = 1 : N-1
        Y = S_min + DS * J;
        for K = 1 : N-1
            Z = S_min + DS * K;
            %
            Field = ((Field_max-Field_min)/S_max) * abs(Z) ...
                  + Field_min;
            fprintf(fileID, '%e %e %e %e\n', X,Y,Z,Field);
        end
    end
end

fclose(fileID);
