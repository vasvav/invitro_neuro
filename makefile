
PROJECT_invitro_neuro = $(shell pwd)
BIODYNAMO_VERSION = 25dc979062d7b4367537c821e80b95e5727f668a
BIODYNAMO_FOLDER  = biodynamo-v1.05.120
CC  = /usr/bin/gcc
CXX = /usr/bin/g++

all:
	@echo "\n *** Please provide a specific rule to execute *** \n"
whereami:
	@echo " ...over here:" $(PROJECT_invitro_neuro)
setup_biodynamo:
	rm -Rf $(PROJECT_invitro_neuro)/libs/biodynamo; \
	git clone https://github.com/BioDynaMo/biodynamo.git $(PROJECT_invitro_neuro)/libs/biodynamo && \
	cd $(PROJECT_invitro_neuro)/libs/biodynamo && \
	git checkout $(BIODYNAMO_VERSION)
install_biodynamo:
	cd $(PROJECT_invitro_neuro)/libs/biodynamo && \
	./prerequisites.sh all && \
	rm -rf build; mkdir build; cd build; \
	cmake -DCMAKE_C_COMPILER=$(CC) -DCMAKE_CXX_COMPILER=$(CXX) \
        -Dparaview=off -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$(PROJECT_invitro_neuro)/libs .. && \
	make -j 4 && make install
source_biodynamo:
	/bin/bash -c 'source $(PROJECT_invitro_neuro)/libs/$(BIODYNAMO_FOLDER)/bin/thisbdm.sh'
clean:
	biodynamo clean && \
	rm -rf $(PROJECT_invitro_neuro)/build
fresh:
	mkdir $(PROJECT_invitro_neuro)/build; \
	cd    $(PROJECT_invitro_neuro)/build; \
	cmake -DCMAKE_C_COMPILER=$(CC) -DCMAKE_CXX_COMPILER=$(CXX) \
        .. && \
	make -j 4
%:
	@echo "\n ******************\n *** Running test: "$@"\n ****************** "; \
	cd $(PROJECT_invitro_neuro)/examples/$@; rm -rf results; make run
check_all_tests:
	make cancer_angiogenesis; \
	make cancer_radiation; \
	make fibroblasts_migration; \
	make HeLa_cells; \
	make invitro_neuro; \
	make neuron_astrocyte; make neuron_bipolar; \
	make obstacle_demo_1; make obstacle_demo_2; \
	make vasculogenesis; \
	make wound_assay; \
	cd $(PROJECT_invitro_neuro); echo "\n ***********************\n *** Completed all tests\n ***********************\n"
tests:
	make clean; make fresh; \
	make -s check_all_tests
